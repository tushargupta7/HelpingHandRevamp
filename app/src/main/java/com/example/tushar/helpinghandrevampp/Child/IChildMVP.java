package com.example.tushar.helpinghandrevampp.Child;

import com.example.tushar.helpinghandrevampp.Enums;
import com.example.tushar.helpinghandrevampp.model.ChildDetailWebResponse;
import com.example.tushar.helpinghandrevampp.model.ChildDetailsDisplay;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tushar on 4/6/17.
 */

public interface IChildMVP {

    /**
     * Presenter operations available from the View
     *      View -> Presenter
     */
    interface PresenterActions{
        void onSwipedRight(int adapterPosition);
        void onSwipedLeft();
        void onSwipedDown();
        void onRefreshed();
        void authenticateTransaction();
        void oncreate();
    }

    /**
     * Interactor operations available from the Presenter
     *      Presenter -> Interactor
     */
    interface InteractorAction{
        void getAllChild();
        int getRemainingCoins();

        void syncSwipeDetails();

        void updateBalance();
    }

    /**
     * Presenter operations available from the Interactor
     *      Interactor -> Presenter
     */

    interface RequiredPresentorAction{
        void onRetrievalSuccess(List<ChildDetailWebResponse> childDetailsArrayListWebResponse);
        void onRetrievalFail(Enums.ErrorCodes errorCodes);
        void onDeletionSuccess();



        interface BalanceUpdateStatus{
            void onBalanceUpdateSuccess();
            void onBalanceUpdateFailiure();
            void onBalanceUpdateError();
        }

    }


    /**
     * View operations available from the Presenter
     *      Presenter -> View
     */
    interface RequiredViewActions{
        void displayChildren(ArrayList<ChildDetailsDisplay> childDetailWebResponses);
        void showErrrorToast(Enums.ErrorCodes errorCodes);
        void removeNeedFromList();

    }
}
