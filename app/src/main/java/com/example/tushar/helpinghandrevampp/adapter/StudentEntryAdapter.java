package com.example.tushar.helpinghandrevampp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.tushar.helpinghandrevampp.R;
import com.example.tushar.helpinghandrevampp.helper.ItemTouchHelperAdapter;
import com.example.tushar.helpinghandrevampp.helper.ItemTouchHelperViewHolder;
import com.example.tushar.helpinghandrevampp.helper.OnStartDragListener;
import com.example.tushar.helpinghandrevampp.model.Child;
import com.example.tushar.helpinghandrevampp.model.ChildDetailsDisplay;
import com.example.tushar.helpinghandrevampp.model.ChildNeed;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class StudentEntryAdapter extends RecyclerView.Adapter<StudentEntryAdapter.ViewHolder> implements ItemTouchHelperAdapter {

    private static List<ChildDetailsDisplay> mStudentEntries;
    private static Context context;
    private final OnStartDragListener mDragStartListener;

    public StudentEntryAdapter(ArrayList<ChildDetailsDisplay> mStudentEntries, Context context, OnStartDragListener dragStartListener) {
        mDragStartListener = dragStartListener;
        this.mStudentEntries = mStudentEntries;
        this.context = context;
    }

    @Override
    public StudentEntryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.child_card_view, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        ChildDetailsDisplay studentEntry = mStudentEntries.get(position);
        Child child=studentEntry.getChild();
        holder.studentAge.setText(child.getChildAge() + " years old");
        holder.studentName.setText(child.getChildName());
        holder.studentAcadScore.setText(child.getChildAcadScore() + " %");
        Picasso.with(context).load(child.getChildImageUrl()).into(holder.studentImage);
        holder.studentDescription.setText(child.getDescription());
        ChildNeed needs = studentEntry.getChildNeed();
        holder.coinsStatus.setText(needs.getCollectedCoins() + "/" + needs.getTotalCoins());
        Picasso.with(context).load(needs.getNeededUrl()).into(holder.neededItemImage);
        // holder.acadProgressBar.setProgress(Integer.parseInt(studentEntry.getChildAcadScore()));
        // Start a drag whenever the handle view it touched
        holder.studentImage.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                mDragStartListener.onStartDrag(holder);

                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return mStudentEntries.size();
    }

    public ChildDetailsDisplay getItem(int position){
        return mStudentEntries.get(position);
    }

    @Override
    public void onItemDismiss(int position) {
        mStudentEntries.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onItemRestored(int position,ChildDetailsDisplay childNeed) {
        mStudentEntries.add(position,childNeed);
        notifyItemInserted(position);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(mStudentEntries, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {
        public ImageView studentImage;
        public TextView studentName;
        public TextView studentAge;
        public TextView studentAcadScore;
        public TextView studentDescription;
        public TextView coinsStatus;
        public ImageView neededItemImage;
        public LinearLayout handleView;

        // public ProgressBar acadProgressBar;
        public ViewHolder(View view) {
            super(view);
            studentImage = (ImageView) view.findViewById(R.id.iv_child_pic);
            studentAge = (TextView) view.findViewById(R.id.tv_child_age);
            studentName = (TextView) view.findViewById(R.id.tv_child_name);
            studentAcadScore = (TextView) view.findViewById(R.id.tv_child_acad_score);
            coinsStatus = (TextView) view.findViewById(R.id.tv_coins_status);
            neededItemImage = (ImageView) view.findViewById(R.id.iv_needed_item);
            studentDescription = (TextView) view.findViewById(R.id.tv_child_desc);
            //acadProgressBar=(ProgressBar)view.findViewById(R.id.pb_child_acad);
            handleView = (LinearLayout) view.findViewById(R.id.ll_child_card_view);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }
}
