package com.example.tushar.helpinghandrevampp.Child;

import android.content.Context;

import com.example.tushar.helpinghandrevampp.Enums;
import com.example.tushar.helpinghandrevampp.HHSharedPreferences;
import com.example.tushar.helpinghandrevampp.R;
import com.example.tushar.helpinghandrevampp.Utils.JsonUtils;
import com.example.tushar.helpinghandrevampp.db.DataBaseHelper;
import com.example.tushar.helpinghandrevampp.db.DatabaseManager;
import com.example.tushar.helpinghandrevampp.model.ChildDetailWebResponse;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tushar on 4/6/17.
 */

public class ChildListInteractor implements IChildMVP.InteractorAction, DatabaseManager.OnBalanceUpdateError {

    private IChildMVP.RequiredPresentorAction mPresentor;
    private IChildMVP.RequiredPresentorAction.BalanceUpdateStatus mBalanceUpdator;

    private Context mContext;
    private DatabaseManager mDataBaseManager;

    public ChildListInteractor(IChildMVP.RequiredPresentorAction presentor, IChildMVP.RequiredPresentorAction.BalanceUpdateStatus balanceUpdateStatus,Context context) {
        mPresentor = presentor;
        mBalanceUpdator=balanceUpdateStatus;
        mContext = context;
        mDataBaseManager = DatabaseManager.getInstance(mContext);
    }

    @Override
    public void getAllChild() {
        /*try {
            CloseableIterator<ChildDetailWebResponse> childDetailesIter=getHelper().getChildDao().iterator();
            while (childDetailesIter.hasNext()){
                ChildDetailWebResponse details=childDetailesIter.current();

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }*/

        List<ChildDetailWebResponse> childDetailWebResponses = getChildListFromWeb();
        mPresentor.onRetrievalSuccess(childDetailWebResponses);
    }

    @Override
    public int getRemainingCoins() {
        return 0;
    }

    @Override
    public void syncSwipeDetails() {

    }

    @Override
    public void updateBalance() {
        int defaultTokenValue = HHSharedPreferences.getInstance(mContext).getDefaultHtv();
        String uuid = HHSharedPreferences.getInstance(mContext).getUserUid();
        mDataBaseManager.updateUserHt(this, uuid, defaultTokenValue);

    }

    public List<ChildDetailWebResponse> getChildListFromWeb() {

        List<ChildDetailWebResponse> childDetailsListedArrayWebResponse = new ArrayList<>();
        try {
            JSONObject childs = new JSONObject(String.valueOf(mContext.getResources().getString(R.string.dummy_json)));
            JSONArray childArray = childs.getJSONArray("student");
            for (int i = 0; i < childArray.length(); i++) {
                JSONObject child = childArray.getJSONObject(i);
                ChildDetailWebResponse childDetailsWebResponse = JsonUtils.fromJsonToObj(child.toString(), ChildDetailWebResponse.class);
                childDetailsListedArrayWebResponse.add(childDetailsWebResponse);
            }
           /* for (int i = 0; i < childArray.length(); i++) {
                JSONObject child=childArray.getJSONObject(i);
                ChildDetailWebResponse childDetails=new ChildDetailWebResponse();

                childDetails.setChildAge(child.getString("age"));
                childDetails.setChildAcadScore(child.getString("acadscore"));
                childDetails.setChildClass(child.getString("cclass"));
                childDetails.setChildName(child.getString("name"));
                childDetails.setDescription(child.getString("description"));
                childDetails.setChildImageUrl(child.getString("picture"));
                JSONArray childNeedsArray=child.getJSONArray("needs");
                for (int j = 0; j < childNeedsArray.length(); j++) {
                    ChildDetailWebResponse duplicateChild=new ChildDetailWebResponse(childDetails);
                    JSONObject childNeedsJsonObject=childNeedsArray.getJSONObject(j);
                    ChildNeed childNeed=new ChildNeed();
                    childNeed.setCollectedCoins(childNeedsJsonObject.getInt("coins_collected"));
                    childNeed.setNeededItem(childNeedsJsonObject.getString("needed_item"));
                    childNeed.setNeededUrl(childNeedsJsonObject.getString("neededurl"));
                    childNeed.setTotalCoins(childNeedsJsonObject.getInt("coins_total"));
                    duplicateChild.setChildNeeds(childNeed);
                    childDetailsListedArrayWebResponse.add(    duplicateChild);
                }
            }*/
        } catch (JSONException e) {
            e.printStackTrace();
            mPresentor.onRetrievalFail(Enums.ErrorCodes.JSON_EXCEPTION);
        }
        return childDetailsListedArrayWebResponse;
    }

    @Override
    public void onBalanceUpdateFinished() {
        mBalanceUpdator.onBalanceUpdateSuccess();
    }

    @Override
    public void onFetchingFailed() {
        mBalanceUpdator.onBalanceUpdateError();
    }

    @Override
    public void onInsufficientBalance() {
        mBalanceUpdator.onBalanceUpdateFailiure();
    }

}
