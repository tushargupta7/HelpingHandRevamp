package com.example.tushar.helpinghandrevampp.model;

/**
 * Created by tushar on 11/6/17.
 */

public class ChildDetailsDisplay {
   

        Child child;

        ChildNeed childNeed;

        public ChildDetailsDisplay(com.example.tushar.helpinghandrevampp.model.ChildDetailWebResponse childDetailsWebResponse) {


        }


    public ChildDetailsDisplay(Child child, ChildNeed childNeed) {
        this.child=child;
        this.childNeed=childNeed;
    }

    public Child getChild() {
        return child;
    }

    public ChildNeed getChildNeed() {
        return childNeed;
    }
}
