package com.example.tushar.helpinghandrevampp.db;

import android.content.Context;

import com.example.tushar.helpinghandrevampp.model.ChildDetailWebResponse;
import com.example.tushar.helpinghandrevampp.model.ChildNeed;
import com.example.tushar.helpinghandrevampp.model.UserDetail;
import com.example.tushar.helpinghandrevampp.model.UserSetting;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by tushar on 7/6/17.
 */


    public class DatabaseManager {

        private static final String TAG = "Database Manager";
        static private DatabaseManager instance;
        private static Context context;


        static public void init(Context ctx) {
            if (null == instance) {
                instance = new DatabaseManager(ctx.getApplicationContext());
                context = ctx.getApplicationContext();
            }
        }

        static public DatabaseManager getInstance(Context ctx) {
            if(null==instance){
                instance = new DatabaseManager(ctx.getApplicationContext());
            }
            context = ctx;
            return instance;
        }

        private DataBaseHelper helper;

        private DatabaseManager(Context ctx) {
            helper = new DataBaseHelper(ctx);
        }

        private DataBaseHelper getHelper() {
            return helper;
        }

        private List<ChildDetailWebResponse> getAllChild(){
            try {
                return getHelper().getChildDao().queryForAll();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        }

        private  List<ChildNeed> getAllNeedsWithChildId(int childId){
            QueryBuilder<ChildNeed, ?> queryBuilder;
            try {
                queryBuilder = getHelper().getDao(ChildNeed.class).queryBuilder();
                return queryBuilder.where().eq(ChildNeed.QUERY_FOREIGN_CHILD_ID, childId).query();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        public void updateUserHt(OnBalanceUpdateError resultCallback, String uuid, int defaultTokenValue){
            UpdateBuilder<UserDetail, ?> updateBuilder;
            QueryBuilder<UserDetail,?> queryBuilder;
            try {
                queryBuilder=getHelper().getUserDao().queryBuilder();
                UserDetail userDetails=queryBuilder.where().eq("user_id",uuid).query().get(0);
                updateBuilder=getHelper().getUserDao().updateBuilder();
                updateBuilder.where().eq("user_id",uuid);
                if(Integer.valueOf(userDetails.getHtBalance())-defaultTokenValue<0){
                    resultCallback.onInsufficientBalance();
                    return;
                }
                updateBuilder.updateColumnValue("user_ht_balance",Integer.valueOf(userDetails.getHtBalance())-defaultTokenValue);
                updateBuilder.updateColumnValue("user_balance_used",userDetails.getHtBalanceUsed()+defaultTokenValue);
                resultCallback.onBalanceUpdateFinished();
            } catch (SQLException e) {
                e.printStackTrace();
                resultCallback.onFetchingFailed();
            }
        }

    public void addDefaultUser() {
        try {
            UserDetail dummyUserDetail;
            dummyUserDetail=new UserDetail("qwertyuiop","tushar","tusharg@gmail.com","7275315822",1000,100,200);
            getHelper().getUserDao().create(dummyUserDetail);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addDefaultToken(){
        try {
            UserSetting dummyUserSettings;
            dummyUserSettings=new UserSetting();
            dummyUserSettings.setDefaultCoin(10);
            getHelper().getUserSettingsDao().create(dummyUserSettings);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public interface OnBalanceUpdateError {
        void onBalanceUpdateFinished();

        void onFetchingFailed();

        void onInsufficientBalance();
    }

}
