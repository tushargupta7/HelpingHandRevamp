package com.example.tushar.helpinghandrevampp.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by tushar on 17/6/17.
 */
@DatabaseTable
public class UserSetting {
    @DatabaseField
    int defaultCoin;

    public int getDefaultCoin() {
        return defaultCoin;
    }

    public void setDefaultCoin(int defaultCoin) {
        this.defaultCoin = defaultCoin;
    }
}
