package com.example.tushar.helpinghandrevampp.Child;

import android.content.Context;
import android.widget.Toast;

import com.example.tushar.helpinghandrevampp.Child.IChildMVP.RequiredPresentorAction;
import com.example.tushar.helpinghandrevampp.Enums;
import com.example.tushar.helpinghandrevampp.model.Child;
import com.example.tushar.helpinghandrevampp.model.ChildDetailWebResponse;
import com.example.tushar.helpinghandrevampp.model.ChildDetailsDisplay;
import com.example.tushar.helpinghandrevampp.model.ChildNeed;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tushar on 4/6/17.
 */

public class ChildListPresenter implements IChildMVP.PresenterActions,RequiredPresentorAction,RequiredPresentorAction.BalanceUpdateStatus {
    private IChildMVP.RequiredViewActions mView;
    private IChildMVP.InteractorAction mInteractor;
    private Context mContext;
    ArrayList<ChildDetailsDisplay> mChildDetailsArray;
    ChildListInteractor childInteractor;
    public ChildListPresenter(IChildMVP.RequiredViewActions view,Context context){
        mInteractor=new ChildListInteractor(this,this,context);
        mView=view;
        mContext=context;
    }
    @Override
    public void onSwipedRight(int adapterPosition) {
        mInteractor.updateBalance();
        mInteractor.syncSwipeDetails();
    }

    @Override
    public void onSwipedLeft() {

    }

    @Override
    public void onSwipedDown() {

    }

    @Override
    public void onRefreshed() {

    }

    @Override
    public void authenticateTransaction() {

    }

    @Override
    public void oncreate() {
    mInteractor.getAllChild();
    }

    @Override
    public void onRetrievalSuccess(List<ChildDetailWebResponse> childDetailsArrayListWebResponse) {
        parseAndGetChildrens(childDetailsArrayListWebResponse);
        mView.displayChildren(mChildDetailsArray);
    }

    private void parseAndGetChildrens(List<ChildDetailWebResponse> childDetailWebResponses) {
        mChildDetailsArray=new ArrayList<ChildDetailsDisplay>();
        for (int i = 0; i < childDetailWebResponses.size(); i++) {
            Child child=childDetailWebResponses.get(i).getChild();
            //Child child=new Child();
            List<ChildNeed> childNeeds=childDetailWebResponses.get(i).getChildNeeds();
            for (int j = 0; j < childNeeds.size(); j++) {
                ChildNeed childNeed=childNeeds.get(j);
                ChildDetailsDisplay childDetailsDisplay=new ChildDetailsDisplay(child,childNeed);
                mChildDetailsArray.add(childDetailsDisplay);
            }
        }
    }

    @Override
    public void onRetrievalFail(Enums.ErrorCodes errorCodes) {
        Toast.makeText(mContext,errorCodes.getErrMsg(),Toast.LENGTH_LONG);
    }

    @Override
    public void onDeletionSuccess() {
        mView.showErrrorToast(Enums.ErrorCodes.AUTHENTICATION_FAILIURE);
    }

    @Override
    public void onBalanceUpdateSuccess() {
       mView.removeNeedFromList();
    }

    @Override
    public void onBalanceUpdateFailiure() {
        mView.showErrrorToast(Enums.ErrorCodes.INSUFFICIENT_BALANCE);
    }

    @Override
    public void onBalanceUpdateError() {

    }
}
