package com.example.tushar.helpinghandrevampp.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;

/**
 * Created by tushar on 17/6/17.
 */
@DatabaseTable
public class UserDetail {
    public static final String USER_ID="user_id";
    public static final String USER_NAME="user_name";
    public static final String MAIL_ID="user_email_id";
    public static final String PHONE_NUMBER="user_phone_num";
    public static final String HT_BALANCE="user_ht_balance";
    public static final String HT_BALANCE_USED="user_balance_used";
    public static final String TOTAL_HT_BALANCE="user_total_balance_used";


    @DatabaseField(id = true , columnName = USER_ID)
    String userId;
    @DatabaseField(columnName = USER_NAME)
    String username;
    @DatabaseField(columnName = MAIL_ID)
    String mailId;
    @DatabaseField(columnName = PHONE_NUMBER)
    String phoneNumber;
    @DatabaseField(columnName = HT_BALANCE)
    int htBalance;
    @DatabaseField(columnName = HT_BALANCE_USED)
    int htBalanceUsed;
    @DatabaseField(columnName = TOTAL_HT_BALANCE)
    int totalHtBalanceUsed;

    public int getHtBalance() {
        return htBalance;
    }

    public int getHtBalanceUsed() {
        return htBalanceUsed;
    }
    public UserDetail(){

    }
    public UserDetail(String uuid, String username, String mailId, String phoneNumber, int htBalance, int htBalanceUsed, int totalHtBalanceUsed){
        this.userId=uuid;
        this.username=username;
        this.mailId=mailId;
        this.phoneNumber=phoneNumber;
        this.htBalance=htBalance;
        this.htBalanceUsed=htBalanceUsed;
        this.totalHtBalanceUsed=totalHtBalanceUsed;
    }
}
