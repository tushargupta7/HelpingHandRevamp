package com.example.tushar.helpinghandrevampp.Child;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tushar.helpinghandrevampp.Enums;
import com.example.tushar.helpinghandrevampp.R;
import com.example.tushar.helpinghandrevampp.adapter.StudentEntryAdapter;
import com.example.tushar.helpinghandrevampp.helper.OnStartDragListener;
import com.example.tushar.helpinghandrevampp.helper.SimpleItemTouchHelperCallback;
import com.example.tushar.helpinghandrevampp.model.ChildDetailWebResponse;
import com.example.tushar.helpinghandrevampp.model.ChildDetailsDisplay;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChildListUIFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChildListUIFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChildListUIFragment extends Fragment implements OnStartDragListener,IChildMVP.RequiredViewActions,SimpleItemTouchHelperCallback.SwipeListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView mRecyclerView;
    private StudentEntryAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ItemTouchHelper mItemTouchHelper;
    private IChildMVP.PresenterActions mPresenter;

    private OnFragmentInteractionListener mListener;
    private int mSwipedLocation;
    private ChildDetailsDisplay mChildDetailsDisplay;

    public ChildListUIFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChildListUIFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChildListUIFragment newInstance(String param1, String param2) {
        ChildListUIFragment fragment = new ChildListUIFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        if (savedInstanceState == null) {
            // Creates presenter
            mPresenter = new ChildListPresenter(this, getActivity());
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_home, container, false);
        // Inflate the layout for this fragment
        mRecyclerView=(RecyclerView) view.findViewById(R.id.rv_children_list);
        //mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mPresenter.oncreate();

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void displayChildren(ArrayList<ChildDetailsDisplay> childDetailsList) {
        mAdapter = new StudentEntryAdapter(childDetailsList,getContext(),this);
        mRecyclerView.setAdapter(mAdapter);

        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(mAdapter,this);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    @Override
    public void showErrrorToast(Enums.ErrorCodes errorCodes) {
        Snackbar snackbar=Snackbar.make(getView(),errorCodes.getErrMsg(),Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void removeNeedFromList() {
        mAdapter.onItemDismiss(mSwipedLocation);
        Snackbar snackbar = Snackbar
                .make(getView(), "Child Need Fulfilled", Snackbar.LENGTH_LONG)
                .setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mAdapter.onItemRestored(mSwipedLocation,mChildDetailsDisplay);
                        Snackbar snackbar1 = Snackbar.make(getView(), "Item is restored!", Snackbar.LENGTH_SHORT);
                        snackbar1.show();
                        mRecyclerView.scrollToPosition(mSwipedLocation);
                    }
                });

        snackbar.show();
    }

    @Override
    public void onSwipe(int adapterPosition) {
        mSwipedLocation=adapterPosition;
        mChildDetailsDisplay=mAdapter.getItem(adapterPosition);
        mPresenter.onSwipedRight(adapterPosition);
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
