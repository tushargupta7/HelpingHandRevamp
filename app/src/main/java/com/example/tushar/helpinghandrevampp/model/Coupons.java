package com.example.tushar.helpinghandrevampp.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by tushar on 17/6/17.
 */
@DatabaseTable
public class Coupons {
    @DatabaseField
    String organisation;
    @DatabaseField
    String couponCOde;
    @DatabaseField
    long expiryTimeStamp;
    @DatabaseField
    boolean used;
    @DatabaseField(foreign = true)
    String userId;
}
