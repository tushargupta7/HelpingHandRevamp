package com.example.tushar.helpinghandrevampp.model;

/**
 * Created by tushar on 3/6/17.
 */

public class OrderDetail {
    int orderId;
    int StudentId;
    String studentName;
    int itemId;
    int tokenMoney;
    long timeStamp;

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getStudentId() {
        return StudentId;
    }

    public void setStudentId(int studentId) {
        StudentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getTokenMoney() {
        return tokenMoney;
    }

    public void setTokenMoney(int tokenMoney) {
        this.tokenMoney = tokenMoney;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
