package com.example.tushar.helpinghandrevampp;

import android.content.Context;
import android.preference.PreferenceManager;


public class HHSharedPreferences {
    public static final String FIRST_LOGIN_ATTEMPT = "FIRST_LOGIN_ATTEMPT";
    public static final String STAY_SIGNED_IN = "STAY_SIGNED_IN";
    public static final String ADMIN_LOGIN = "ADMIN_LOGIN";
    public static final String USER_UID="uuid";
    public static final String USER_TOKEN="token";
    private static final String AUTO_LOGIN = "false";
    private static final String USER_NAME="user_name";
    private static final String USER_EMAIL = "e_mail";
    private static final String DEFAULT_HTV= "default_ht";
    private static HHSharedPreferences mInstance;
    private Context mContext;
    private android.content.SharedPreferences mPrefs;

    private HHSharedPreferences(Context applicationContext) {
        mContext = applicationContext;
        mPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public static HHSharedPreferences getInstance(Context ctxt) {
        if (mInstance == null) {
            mInstance = new HHSharedPreferences(ctxt.getApplicationContext());
        }

        return mInstance;
    }

    public void writePreference(String key, String value) {
        android.content.SharedPreferences.Editor e = mPrefs.edit();
        e.putString(key, value);
        e.apply();
    }

    public void writePreference(String key, boolean value) {
        android.content.SharedPreferences.Editor e = mPrefs.edit();
        e.putBoolean(key, value);
        e.apply();
    }

    public void writePreference(String key, int value) {
        android.content.SharedPreferences.Editor e = mPrefs.edit();
        e.putInt(key, value);
        e.apply();
    }

    public void setUserUid(String value){
        writePreference(USER_UID,value);
    }
    public void setAutoLogin(boolean value){
        writePreference(AUTO_LOGIN,value);
    }

    public boolean isAutoLogin(){
        return mPrefs.getBoolean(AUTO_LOGIN,false);
    }

    public void setUserToken(String value){
        writePreference(USER_TOKEN,value);
    }

    public void setUserName(String value){
        writePreference(USER_NAME,value);
    }

    public void setUserEmail(String value){
        writePreference(USER_EMAIL,value);
    }

    public String getUserUid(){
       return mPrefs.getString(USER_UID,null);
    }

    public String getUserToken(){
        return mPrefs.getString(USER_TOKEN,null);
    }
    public void setLoggeInAsAdmin(boolean value) {
        writePreference(ADMIN_LOGIN, value);
    }

    public boolean getLoggedInAsAdmin() {
        return mPrefs.getBoolean(ADMIN_LOGIN, false);
    }
    public String getUserName(){
        return mPrefs.getString(USER_NAME,null);
    }
    public String getUserEmail(){
        return mPrefs.getString(USER_EMAIL,null);
    }

    public void setDefaultHtv(int happyTokenValue){
        writePreference(DEFAULT_HTV,happyTokenValue);
    }
    public int getDefaultHtv(){
        return mPrefs.getInt(DEFAULT_HTV,1);
    }
}
