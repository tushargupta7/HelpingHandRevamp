package com.example.tushar.helpinghandrevampp.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by tushar on 29/5/17.
 */

@DatabaseTable
public class ChildNeed {
    public static final String QUERY_FOREIGN_CHILD_ID="childId";
    @DatabaseField
    String neededItem;
    @DatabaseField
    String neededUrl;
    @DatabaseField
    int collectedCoins;
    @DatabaseField
    int totalCoins;
    @DatabaseField
    boolean displayed;

    @DatabaseField(foreign = true)
    int childId;

    public boolean isDisplayed() {
        return displayed;
    }

    public void setDisplayed(boolean displayed) {
        this.displayed = displayed;
    }


    public String getNeededItem() {
        return neededItem;
    }

    public void setNeededItem(String neededItem) {
        this.neededItem = neededItem;
    }

    public String getNeededUrl() {
        return neededUrl;
    }

    public void setNeededUrl(String neededUrl) {
        this.neededUrl = neededUrl;
    }

    public int getCollectedCoins() {
        return collectedCoins;
    }

    public void setCollectedCoins(int collectedCoins) {
        this.collectedCoins = collectedCoins;
    }

    public int getTotalCoins() {
        return totalCoins;
    }

    public void setTotalCoins(int totalCoins) {
        this.totalCoins = totalCoins;
    }
}
