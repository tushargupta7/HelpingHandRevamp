package com.example.tushar.helpinghandrevampp.db;

import android.content.Context;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.tushar.helpinghandrevampp.model.ChildDetailWebResponse;
import com.example.tushar.helpinghandrevampp.model.ChildNeed;
import com.example.tushar.helpinghandrevampp.model.Coupons;
import com.example.tushar.helpinghandrevampp.model.UserDetail;
import com.example.tushar.helpinghandrevampp.model.UserSetting;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by tushar on 5/6/17.
 */

public class DataBaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String DATABASE_NAME = "notify.db";
    private static final int DATABASE_VERSION = 4;
    private final String LOG_NAME = getClass().getName();

    private Dao<ChildDetailWebResponse, Integer> childDao;
    private Dao<ChildNeed, Integer> childNeedDao;
    private Dao<UserDetail, Integer> userDao;
    private Dao<Coupons, Integer> couponDao;
    private Dao<UserSetting,Integer> userSettingsDao;



    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, UserDetail.class);
            TableUtils.createTable(connectionSource, UserSetting.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int oldVersion,
                          int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, ChildDetailWebResponse.class, true);
            TableUtils.dropTable(connectionSource, ChildNeed.class, true);

            onCreate(sqLiteDatabase, connectionSource);
        } catch (SQLException e) {
            Log.e(LOG_NAME, "Could not upgrade the table for Thing", e);
        }
    }

    public Dao<ChildDetailWebResponse, Integer> getChildDao() throws SQLException {
        if (childDao == null) {
            childDao = getDao(ChildDetailWebResponse.class);
        }
        return childDao;
    }
    public Dao<ChildNeed,Integer> getNeedsDao() throws SQLException{
        if (childNeedDao == null) {
            childNeedDao= getDao(ChildNeed.class);
        }
        return childNeedDao;
    }

    public Dao<UserDetail,Integer> getUserDao() throws SQLException{
        if (userDao==null){
            userDao=getDao(UserDetail.class);
        }
        return userDao;
    }

    public Dao<Coupons,Integer> getCouponDao() throws SQLException{
        if (couponDao==null){
            couponDao=getDao(Coupons.class);
        }
        return couponDao;
    }

    public Dao<UserSetting,Integer> getUserSettingsDao() throws SQLException{
        if (userSettingsDao==null){
            userSettingsDao=getDao(UserSetting.class);
        }
        return userSettingsDao;
    }



}
