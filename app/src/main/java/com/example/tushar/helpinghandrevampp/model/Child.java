package com.example.tushar.helpinghandrevampp.model;

/**
 * Created by tushar on 11/6/17.
 */

public class Child {

    String childId;
    String childName;
    String guid;
    String childClass;
    String childGender;
    String childAcadScore;
    String childAge;
    String childEmail;
    String description;
    String childImageUrl;

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    public String getChildName() {
        return childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }

    public String getChildGender() {
        return childGender;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public void setChildGender(String childGender) {
        this.childGender = childGender;
    }

    public String getChildEmail() {
        return childEmail;
    }

    public void setChildEmail(String childEmail) {
        this.childEmail = childEmail;
    }

    public String getChildClass() {
        return childClass;
    }

    public void setChildClass(String childClass) {
        this.childClass = childClass;
    }

    public String getChildAcadScore() {
        return childAcadScore;
    }

    public void setChildAcadScore(String childAcadScore) {
        this.childAcadScore = childAcadScore;
    }


    public String getChildAge() {
        return childAge;
    }

    public void setChildAge(String childAge) {
        this.childAge = childAge;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getChildImageUrl() {
        return childImageUrl;
    }

    public void setChildImageUrl(String childImageUrl) {
        this.childImageUrl = childImageUrl;
    }
}
