package com.example.tushar.helpinghandrevampp;

/**
 * Created by tushar on 4/6/17.
 */
public class Enums {
    public enum ErrorCodes{


        AUTHENTICATION_FAILIURE("101","Please Sign In Again"),
        JSON_EXCEPTION("102","Json Exception"),
        INSUFFICIENT_BALANCE("","Insufficient Balance");
        String errCode,errMsg;
        ErrorCodes(String errorCode, String errorMsg) {
            errCode=errorCode;
            errorMsg=errorMsg;
        }

        public String getErrorCode() {
            return errCode;
        }

        public void setErrorCode(String errorCode) {
            this.errCode = errorCode;
        }

        public String getErrMsg() {
            return errMsg;
        }

        public void setErrMsg(String errMsg) {
            this.errMsg = errMsg;
        }
    }
}
