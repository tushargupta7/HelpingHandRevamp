package com.example.tushar.helpinghandrevampp.model;

import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

/**
 * Created by tushar on 1/4/16.
 */
public class ChildDetailWebResponse implements Cloneable {
    Child child;

    List<ChildNeed> childNeeds;

    public ChildDetailWebResponse(ChildDetailWebResponse childDetailsWebResponse) {

    }

    public ChildDetailWebResponse() {

    }

    public Child getChild() {
        return child;
    }

    public void setChild(Child child) {
        this.child = child;
    }

    public List<ChildNeed> getChildNeeds() {
        return childNeeds;
    }

    public void setChildNeeds(List<ChildNeed> childNeeds) {
        this.childNeeds = childNeeds;
    }
}
