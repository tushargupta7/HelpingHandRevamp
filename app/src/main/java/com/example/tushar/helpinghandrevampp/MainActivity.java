package com.example.tushar.helpinghandrevampp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.MenuItem;
import com.example.tushar.helpinghandrevampp.Child.ChildListUIFragment;
import com.example.tushar.helpinghandrevampp.adapter.StudentEntryAdapter;
import com.example.tushar.helpinghandrevampp.db.DatabaseManager;

public class MainActivity extends AppCompatActivity implements ChildListUIFragment.OnFragmentInteractionListener{
    private RecyclerView mRecyclerView;
    private StudentEntryAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ItemTouchHelper mItemTouchHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUser();
        FragmentManager fm=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fm.beginTransaction();
        ChildListUIFragment fragment = new ChildListUIFragment();
        fragmentTransaction.add(R.id.fragment_container, fragment);
        fragmentTransaction.commit();
        /*mRecyclerView=(RecyclerView) findViewById(R.id.rv_children_list);
        //mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)

        ArrayList<ChildDetailWebResponse> childDetailsListedArray=new ArrayList<>();
        try {
            JSONObject childs=new JSONObject(String.valueOf(getResources().getString(R.string.dummy_json)));
            JSONArray childArray=childs.getJSONArray("student");
            for (int i = 0; i < childArray.length(); i++) {
                JSONObject child=childArray.getJSONObject(i);
                ChildDetailWebResponse childDetails=new ChildDetailWebResponse();

                childDetails.setChildAge(child.getString("age"));
                childDetails.setChildAcadScore(child.getString("acadscore"));
                childDetails.setChildClass(child.getString("cclass"));
                childDetails.setChildName(child.getString("name"));
                childDetails.setDescription(child.getString("description"));
                childDetails.setChildImageUrl(child.getString("picture"));
                JSONArray childNeedsArray=child.getJSONArray("needs");
                for (int j = 0; j < childNeedsArray.length(); j++) {
                    ChildDetailWebResponse duplicateChild=new ChildDetailWebResponse(childDetails);
                    JSONObject childNeedsJsonObject=childNeedsArray.getJSONObject(j);
                    ChildNeedsCardViewPopulate childNeed=new ChildNeedsCardViewPopulate();
                    childNeed.setCollectedCoins(childNeedsJsonObject.getInt("coins_collected"));
                    childNeed.setNeededItem(childNeedsJsonObject.getString("needed_item"));
                    childNeed.setNeededUrl(childNeedsJsonObject.getString("neededurl"));
                    childNeed.setTotalCoins(childNeedsJsonObject.getInt("coins_total"));
                    duplicateChild.setChildNeeds(childNeed);
                    childDetailsListedArray.add(duplicateChild);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mAdapter = new `StudentEntryAdapter(childDetailsListedArray,this,this);
        mRecyclerView.setAdapter(mAdapter);

        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(mAdapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(mRecyclerView);*/
    }

    private void initUser() {
        HHSharedPreferences.getInstance(this).setUserUid("qwertyuiop");
        DatabaseManager dbManager=DatabaseManager.getInstance(this);
        dbManager.addDefaultUser();
        dbManager.addDefaultToken();
    }


    public void onHomeClicked(MenuItem item){
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
